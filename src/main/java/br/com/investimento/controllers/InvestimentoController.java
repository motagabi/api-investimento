package br.com.investimento.controllers;

import br.com.investimento.DTOs.SimulacaoDTO;
import br.com.investimento.models.Investimento;
import br.com.investimento.models.Simulacao;
import br.com.investimento.services.InvestimentoService;
import br.com.investimento.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {
    @Autowired
    private InvestimentoService investimentoService;

    @Autowired
    private SimulacaoService simulacaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Investimento registrarInvestimento(@RequestBody @Valid Investimento investimento) {
        return investimentoService.salvarInvestimento(investimento);
    }

    @GetMapping
    public Iterable<Investimento> listarOsInvestimentos(){
        return investimentoService.lerTodosOsInvestimentos();
    }

    @GetMapping("/{id}")
    public Investimento retornaTodosOsInvestimentos(@RequestBody @PathVariable int id){
        return investimentoService.buscarInvestimentoPorId(id);
    }

    @PostMapping("/{idInvestimento}/simulacao")
    @ResponseStatus(HttpStatus.CREATED)
    public SimulacaoDTO simularInvestimento(@RequestBody @Valid Simulacao simulacao, @PathVariable(name = "idInvestimento") int idInvestimento){
        return simulacaoService.salvarSimulacaoDTO(simulacao, idInvestimento);

    }


}
