package br.com.investimento.services;

import br.com.investimento.models.Investimento;
import br.com.investimento.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Investimento salvarInvestimento(Investimento investimento){
        Investimento objetoProduto = investimentoRepository.save(investimento);
        return objetoProduto;
    }


    public Iterable<Investimento> lerTodosOsInvestimentos(){
        return investimentoRepository.findAll();
    }

    public Investimento buscarInvestimentoPorId(int idInvestimento){

        Optional<Investimento> investimentoOptional = investimentoRepository.findById(idInvestimento);

        if(investimentoOptional.isPresent()){
            Investimento investimento = investimentoOptional.get();
            return investimento;
        }else {
            throw new RuntimeException("O investimento não foi encontrado!");
        }
    }


}
