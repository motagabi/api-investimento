package br.com.investimento.services;

import br.com.investimento.DTOs.SimulacaoDTO;
import br.com.investimento.models.Investimento;
import br.com.investimento.models.Simulacao;
import br.com.investimento.repositories.InvestimentoRepository;
import br.com.investimento.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

@Service
public class SimulacaoService {
    @Autowired
    private InvestimentoRepository investimentoRepository;

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    public SimulacaoDTO salvarSimulacaoDTO(Simulacao simulacao, int idInvestimento) {

        SimulacaoDTO retornoSimulacaoDTO = new SimulacaoDTO();
        Optional<Investimento> investimentoOptional = investimentoRepository.findById(idInvestimento);

        if (investimentoOptional.isPresent()) {
            Investimento investimento = investimentoOptional.get();

            simulacao.setInvestimento(investimento);
            simulacaoRepository.save(simulacao);

            return retornoSimulacaoDTO = this.SimularInvestimento(simulacao.getValorAplicacao(), simulacao.getQuantidadeMeses(),investimento.getRendimentoAoMes());

        } else {

            throw new RuntimeException("Investimento informado não exite");

        }
    }

    private SimulacaoDTO SimularInvestimento(double valorAplicacao, int quantidaDeMeses, double taxaAoMes) {

        SimulacaoDTO retornoSimulacaoDTO = new SimulacaoDTO();
        retornoSimulacaoDTO.setRendimentoPorMes(taxaAoMes);

        double resultadoMontante = valorAplicacao * Math.pow (1 + (taxaAoMes/100), quantidaDeMeses);

        BigDecimal montanteFormatado = new BigDecimal(resultadoMontante);
        montanteFormatado = montanteFormatado.setScale(2, RoundingMode.UP);
        retornoSimulacaoDTO.setMontante(montanteFormatado.doubleValue());

        return retornoSimulacaoDTO;
    }

    public Iterable<Simulacao> retornarTodasAsSimulacoes() {
        return simulacaoRepository.findAll();
    }

}
