package br.com.investimento.models;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "investimento")

public class Investimento {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotNull
    @NotBlank(message = "Não pode estar em branco")
    @Size(min=3, message = "Nome mínimo 3 caracteres")
    @Column(unique = true)
    private String nome;

    @Digits(integer = 6, fraction = 2, message = "Preço fora do padrão")
    private double rendimentoAoMes;


    public Investimento() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }
}
